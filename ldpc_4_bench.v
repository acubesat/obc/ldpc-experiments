`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:04:17 12/25/2018 
// Design Name: 
// Module Name:    ldpc_4_bench 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ldpc_4_bench(
    input clk,
    input start,
    input wea,
    output [31:0] paritybits,
    input [2:0] parityaddr,
	 input [3:0] dataaddr,
    input [63:0] datain,
    input clkm//,
	 //output [31:0] 
    );
	

	wire [255:0] parity;
	
	ldpc4 ldpc4i (
		clk, start, parity, dataaddr, datain, wea, clkm
	);
	
	assign paritybits = paritygen(parityaddr);
	
	function [31:0] paritygen(input [2:0] addr);
		//paritygen = parity[((addr+1)*32-1):(addr*32)];
		case (addr)
			3'b000 : paritygen = parity[31:0];
			3'b001 : paritygen = parity[63:32];
			3'b010 : paritygen = parity[95:64];
			3'b011 : paritygen = parity[127:96];
			3'b100 : paritygen = parity[159:128];
			3'b101 : paritygen = parity[191:160];
			3'b110 : paritygen = parity[223:192];
			3'b111 : paritygen = parity[255:224];
		endcase
	endfunction


endmodule

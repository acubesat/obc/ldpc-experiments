`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:48:53 12/27/2018
// Design Name:   ldpc4
// Module Name:   /home/kongr45gpen/fpga/ldpc1/ldpc4_sm_test.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ldpc4
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ldpc4_sm_test;

	// Inputs
	reg clk;
	reg start;
	reg [3:0] addrind;
	reg [63:0] dind;
	reg wead;
	reg clkd;

	// Outputs
	wire [255:0] parity;

	// Instantiate the Unit Under Test (UUT)
	ldpc4 uut (
		.clk(clk), 
		.start(start), 
		.parity(parity), 
		.addrind(addrind), 
		.dind(dind), 
		.wead(wead), 
		.clkd(clkd)
	);

	initial begin
		// Initialize Inputs
		clk = 1;
		start = 0;
		addrind = 0;
		dind = 0;
		wead = 0;
		clkd = 0;
		
		// First, load the data
		wead = 1;
		repeat(16) begin
			case (addrind)
				16'h0: dind = 64'h4c44504320697320;
				16'h1: dind = 64'h6120706172697479;
				16'h2: dind = 64'h20636865636b2063;
				16'h3: dind = 64'h6f64652072656c79;
				16'h4: dind = 64'h696e67206f6e2073;
				16'h5: dind = 64'h7061727365207061;
				16'h6: dind = 64'h726974792d636865;
				16'h7: dind = 64'h636b206d61747269;
				16'h8: dind = 64'h6365732e20506172;
				16'h9: dind = 64'h6974792063686563;
				16'ha: dind = 64'h6b20636f64657320;
				16'hb: dind = 64'h6164642070617269;
				16'hc: dind = 64'h7479206269747320;
				16'hd: dind = 64'h746f20746865206f;
				16'he: dind = 64'h7574676f696e6720;
				default: dind = 64'h6d6573736167652e;
			endcase
			//dind = 64'h0;
			#10 clkd = 1;
			#10 clkd = 0;
			addrind = addrind + 1;
		end
		wead = 0;

		// Wait 100 ns for global reset to finish
		#2;
		uut.state = uut.FETCH;
        
		// Add stimulus here
		repeat(5000) begin
			#1 clk = !clk;
			#1 clk = !clk;
		end
	end
      
endmodule


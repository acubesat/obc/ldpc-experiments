`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:56:56 12/24/2018
// Design Name:   genmem
// Module Name:   /home/kongr45gpen/fpga/ldpc1/genmem_test.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: genmem
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module genmem_test;

	// Inputs
	reg clka;
	reg [6:0] addra;

	// Outputs
	wire [63:0] douta;

	// Instantiate the Unit Under Test (UUT)
	genmem uut (
		.clka(clka), 
		.addra(addra), 
		.douta(douta)
	);
	
	always #1 clka = !clka;
	always #2 addra = addra + 1;

	initial begin
		// Initialize Inputs
		clka = 0;
		addra = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		$finish;
	end
      
endmodule


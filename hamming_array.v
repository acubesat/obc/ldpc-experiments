`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:29:56 12/08/2018 
// Design Name: 
// Module Name:    hamming_array 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module hamming_array(
    input CLK,
    input [0:3] data,
    output reg [0:2] parity
    );

`include "hamming_G.v"

integer i;
integer j;

always @(posedge CLK)
begin
	

	for (i = 0; i <= 2; i = i + 1)
	begin
		parity[i] = 0;
		for (j = 0; j <= 3; j = j + 1)
		begin
			parity[i] = parity[i] ^ (Gmatrix[4 * i + j] && data[j]);
		end
	end
end

endmodule

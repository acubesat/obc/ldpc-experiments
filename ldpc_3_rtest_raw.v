`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   04:27:54 12/26/2018
// Design Name:   ldpc3
// Module Name:   /home/kongr45gpen/fpga/ldpc1/ldpc_3_rtest_raw.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ldpc3
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ldpc_3_rtest_raw;

	// Inputs
	reg clk;
	reg start;
	reg weam;
	reg [3:0] addrdin;
	reg [63:0] dinm;
	reg clkmin;

	// Outputs
	wire [255:0] parity;

	// Instantiate the Unit Under Test (UUT)
	ldpc3 uut (
		.clk(clk), 
		.start(start), 
		.parity(parity), 
		.weam(weam), 
		.addrdin(addrdin), 
		.dinm(dinm), 
		.clkmin(clkmin)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		start = 0;
		weam = 0;
		addrdin = 0;
		dinm = 0;
		clkmin = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// First, load the data
		weam = 1;
		repeat(16) begin
			case (addrdin)
				16'h0: dinm = 64'h4c44504320697320;
				16'h1: dinm = 64'h6120706172697479;
				16'h2: dinm = 64'h20636865636b2063;
				16'h3: dinm = 64'h6f64652072656c79;
				16'h4: dinm = 64'h696e67206f6e2073;
				16'h5: dinm = 64'h7061727365207061;
				16'h6: dinm = 64'h726974792d636865;
				16'h7: dinm = 64'h636b206d61747269;
				16'h8: dinm = 64'h6365732e20506172;
				16'h9: dinm = 64'h6974792063686563;
				16'ha: dinm = 64'h6b20636f64657320;
				16'hb: dinm = 64'h6164642070617269;
				16'hc: dinm = 64'h7479206269747320;
				16'hd: dinm = 64'h746f20746865206f;
				16'he: dinm = 64'h7574676f696e6720;
				default: dinm = 64'h6d6573736167652e;
			endcase
			#10 clkmin = 1;
			#10 clkmin = 0;
			addrdin = addrdin + 1;
		end
		weam = 0;
        
		// Add stimulus here
		$finish;
	end
      
endmodule


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:11:35 12/23/2018 
// Design Name: 
// Module Name:    ldpc_2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ldpc_2(
    input CLK,
	 input [63:0] mdata,
	 output reg [4:0] maddr,
    output reg [255:0] parity
    );

`include "ldpc_2_generator.v"

// Each rotation of the generator circulants
integer offset = 0;

// Each row of the circulants
integer crow = 0;

integer bit;

initial parity = 0;

reg databit;

always @(posedge CLK)
begin
	bit = crow * B + offset;

	// Access the data bit
	maddr = bit/64;
	databit = mdata[bit % 64];
	if (databit) begin
		parity[bit % M] = parity[bit % M] ^ 1;
	end

	//crow = crow + 1;
	offset = offset + 1;
	
	/*
	if (crow >= K/B) begin
		if (offset < B) begin
			offset = offset + 1;
			crow = 0;
		end
	end
	*/
end

/*
function [63:0] getCirculant;
input integer i;
begin
	getCirculant = i * i * i;
	// TODO: Reverse fetching order
	// getCirculant = (generator >> (64 * i)) & {64{1'b1}};
end
endfunction
*/

endmodule

`timescale 1ns / 1ps

`define B 32
`define K 1024
`define R 256
`define RL (`R/64)



//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:25:13 12/24/2018 
// Design Name: 
// Module Name:    ldpc3 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ldpc3(
    input clk,
	 input start,
    output reg [(`R-1):0] parity,
	 input weam,
	 input [3:0] addrdin,
	 input [63:0] dinm,
	 input clkmin
    );

// State of the machine
reg [1:0] state;

// State machine states
parameter IDLE = 2'b00;
parameter ERROR = 2'b01;
parameter PARITY = 2'b10;
parameter ROTATE = 2'b11;

// Stage of the PARITY pipeline
// 0: fetching data
// 1: processing
reg stage;

// Iteration variables
integer offset, crow, idx, cidx;

// Memories
reg clkg = 0; // clkg works on posedge
wire [63:0] douta;
reg [6:0] addrg;
genmem generatormem (
  .clka(clkg), // input clka
  .addra(addrg), // input [6 : 0] addra
  .douta(douta) // output [63 : 0] douta
);

wire [63:0] doutm;
reg clkm = 0;
reg [3:0] addrd;
datamem datamemory (
  .clka(weam ? clkmin : clkm), // input clka
  .wea(weam), // input [0 : 0] wea
  .addra(weam ? addrdin : addrd), // input [3 : 0] addra
  .dina(dinm), // input [63 : 0] dina
  .douta(doutm) // output [63 : 0] douta
);
/*
initial begin
	state = 0;
	stage = 0;
	
	parity = 0;
	
	// Initialize all loop variables to zero
	offset = 0;
	idx = 0;
	cidx = 0;
	crow = 0;
end*/

// Registers for temporary data storage
reg [9:0] bitPos; // position of the data bit
reg datum; // One bit of data
reg [63:0] circ; // 64 bits of the circulant

integer i, j;

// TODO: See if >= is better than == for variable checks
always @(negedge clk)
begin
	if (start) begin
		state = 2'b10 ; // PARITY
		stage = 0;
		
		parity = 0;
		
		// Initialize all loop variables to zero
		offset = 0;
		idx = 0;
		cidx = 0;
		crow = 0;
		
		addrg = 0;
		addrd = 0;
	end
	else if (state == 2'b10) begin
		//$display("[off,crow,idx] = [%d,%d,%d]", offset, crow, idx);
		
		if (stage == 0) begin
			// Pipeline stage 0: Data fetching
			
			// Drop the clock
			clkg = 1;
			clkm = 1;
			
			// Move to the next stage
			stage = 1;
		end
		else begin
			// Pipeline stage 1: Parity calculation
			
			// Fetch data from memories, which should have
			// latched at this point
			circ = douta;
			//datum = doutm[(((bitPos % 64)/8)*8) + (7-bitPos%8)];
			datum = doutm[8*(7-(bitPos % 64)/8) + (7 - (bitPos % 8))];
			/*for (j = 0; j < 64; j = j + 1) begin
				$display("The character I monitor is: %s[%h] %d %b", (doutm), doutm, j, doutm[j]);
			end*/
			
			// Calculate the parity bits
			//if (bitPos <= 31)
				//$display("The character I cherish is: %s[%h] %d %b", (doutm), doutm, bitPos, datum == 1);
			//if (datum == 1) begin
				for (i = 0; i < 64; i = i + 1) begin
					//parity[idx * 64 + i] = parity[idx * 64 + i] ^ circ[i];
					parity[(`RL - idx - 1) * 64 + i] = parity[(`RL - idx - 1) * 64 + i] ^ (circ[i] & datum);
				end
			//end
			
			// Increase indexes
			idx = idx + 1;
			cidx = cidx + 1;
			if (idx == `RL) begin
				// Circulant iteration done
				crow = crow + 1;
				
				if (crow == `K/`B) begin
					// Circulant row iteration done
					
					// Now move on to the rotation
					state = (ROTATE);
					crow = 0;
					
					// TODO: Perhaps changing the state is not required
					// for this
				end
				
				idx = 0;
				cidx = crow * `RL;
			end
			
			// Move to the next pipeline stage
			stage = 0;
			addrg = cidx; // Set circulant output
			bitPos = crow * `B + offset;
			addrd = bitPos / 64;
			clkg = 0;
			clkm = 0;
		end
	end
	else if (state == 2'b11) begin
		$display("[off,block] = [%d,%d]", offset, crow);
		
		// Here we only do a left rotation of the parity word
		// TODO: Fix this mess
		parity[((0+1)*32-1) : ((0)*32)] = parity[(0+1)*32-1] | (parity[((0+1)*32-1) : ((0)*32)] << 1);
		parity[((1+1)*32-1) : ((1)*32)] = parity[(1+1)*32-1] | (parity[((1+1)*32-1) : ((1)*32)] << 1);
		parity[((2+1)*32-1) : ((2)*32)] = parity[(2+1)*32-1] | (parity[((2+1)*32-1) : ((2)*32)] << 1);
		parity[((3+1)*32-1) : ((3)*32)] = parity[(3+1)*32-1] | (parity[((3+1)*32-1) : ((3)*32)] << 1);
		parity[((4+1)*32-1) : ((4)*32)] = parity[(4+1)*32-1] | (parity[((4+1)*32-1) : ((4)*32)] << 1);
		parity[((5+1)*32-1) : ((5)*32)] = parity[(5+1)*32-1] | (parity[((5+1)*32-1) : ((5)*32)] << 1);
		parity[((6+1)*32-1) : ((6)*32)] = parity[(6+1)*32-1] | (parity[((6+1)*32-1) : ((6)*32)] << 1);
		parity[((7+1)*32-1) : ((7)*32)] = parity[(7+1)*32-1] | (parity[((7+1)*32-1) : ((7)*32)] << 1);
		
		// Increase the indexes
		offset = offset + 1;
		bitPos = crow * `B + offset;
		if (offset == `B) begin
			// Final offset; calculations done
			$display("Iteration done; Setting to IDLE");
			state = IDLE;
		end
		else begin
			state = PARITY;
		end
	end
	else begin
		// Idle
	end
end


endmodule

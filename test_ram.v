`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:45:36 12/23/2018
// Design Name:   datamem
// Module Name:   /home/kongr45gpen/fpga/ldpc1/test_ram.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: datamem
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_ram;

	// Inputs
	reg clka;
	reg [0:0] wea;
	reg [3:0] addra;
	reg [63:0] dina;

	// Outputs
	wire [63:0] douta;

	// Instantiate the Unit Under Test (UUT)
	datamem uut (
		.clka(clka), 
		.wea(wea), 
		.addra(addra), 
		.dina(dina), 
		.douta(douta)
	);

	initial begin
		// Initialize Inputs
		clka = 0;
		wea = 0;
		addra = 0;
		dina = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// First, load the ram
		wea = 1;
		repeat (16) begin
			dina = $random;
			#1 addra = addra + 1;
			#1 clka = !clka;
		end
		wea = 0;
        
		// Add stimulus here
		repeat (100) begin
			#1 addra = addra + 1;
			#1 clka = !clka;
		end

	end
      
endmodule


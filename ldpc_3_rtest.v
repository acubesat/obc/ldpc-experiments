`timescale 1ns / 10ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   04:02:59 12/26/2018
// Design Name:   ldpc_3_bench
// Module Name:   /home/kongr45gpen/fpga/ldpc1/ldpc_3_rtest.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ldpc_3_bench
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ldpc_3_rtest;

	// Inputs
	reg clk;
	reg start;
	reg wea;
	reg [2:0] parityaddr;
	reg [3:0] dataaddr;
	reg [63:0] datain;
	reg clkm;

	// Outputs
	wire [31:0] paritybits;

	// Instantiate the Unit Under Test (UUT)
	ldpc_3_bench uut (
		.clk(clk), 
		.start(start), 
		.wea(wea), 
		.paritybits(paritybits), 
		.parityaddr(parityaddr), 
		.dataaddr(dataaddr), 
		.datain(datain), 
		.clkm(clkm)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		start = 0;
		wea = 0;
		parityaddr = 0;
		dataaddr = 0;
		datain = 0;
		clkm = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// First, load the data
		wea = 1;
		repeat(16) begin
			case (dataaddr)
				16'h0: datain = 64'h4c44504320697320;
				16'h1: datain = 64'h6120706172697479;
				16'h2: datain = 64'h20636865636b2063;
				16'h3: datain = 64'h6f64652072656c79;
				16'h4: datain = 64'h696e67206f6e2073;
				16'h5: datain = 64'h7061727365207061;
				16'h6: datain = 64'h726974792d636865;
				16'h7: datain = 64'h636b206d61747269;
				16'h8: datain = 64'h6365732e20506172;
				16'h9: datain = 64'h6974792063686563;
				16'ha: datain = 64'h6b20636f64657320;
				16'hb: datain = 64'h6164642070617269;
				16'hc: datain = 64'h7479206269747320;
				16'hd: datain = 64'h746f20746865206f;
				16'he: datain = 64'h7574676f696e6720;
				default: datain = 64'h6d6573736167652e;
			endcase
			#10 clkm = 1;
			#10 clkm = 0;
			dataaddr = dataaddr + 1;
		end
		wea = 0;
		
		start = 1;
		#20 clk = !clk;
		#20 clk = !clk;
		start = 0;
		
		repeat (30000) begin
			#20 clk = !clk;
		end
        
		// Add stimulus here
		$finish;
	end
      
endmodule


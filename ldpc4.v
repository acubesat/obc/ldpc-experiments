`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:53:46 12/27/2018 
// Design Name: 
// Module Name:    ldpc4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ldpc4 #(
  parameter DATA_WIDTH = 64, // Width of the data memory
  parameter DATA_DEPTH = 4, // Address bits of the data memory
  parameter CIRCULANT_WIDTH = 64, // Width of the circulant memory
  parameter K = 1024, // Number of data bits
  parameter M = 256, // Number of parity bits
  parameter C = 32 // Circulant size TODO: Infer from other data
)(
    input clk,
    input start,
    output reg [255:0] parity,
    input [3:0] addrind,
    input [63:0] dind,
    input wead,
    input clkd
    );

// Memories
// Memory for the generator matrix
wire clkg; // clkg works on posedge
wire [63:0] doutg;
reg [63:0] douta;
reg [6:0] addrg = 0;
genmem generatormem (
  .clka(clkg), // input clka
  .addra(addrg), // input [6 : 0] addra
  .douta(doutg) // output [63 : 0] douta
);

// Memory for the data matrix
wire [63:0] doutm;
wire clkm;
reg [3:0] addrd = 0;
datamem datamemory (
  .clka(wead ? clkd : clkm), // input clka
  .wea(wead), // input [0 : 0] wea
  .addra(wead ? addrind : addrd), // input [3 : 0] addra
  .dina(dind), // input [63 : 0] dina
  .douta(doutm) // output [63 : 0] douta
);

// State machine
localparam IDLE    = 3'b001;
localparam FETCH   = 3'b010;
localparam PROCESS = 3'b100;
reg [2:0] state = IDLE;

// Implementation

// Shift registers for the circulants
reg [(C-1):0] circulants [(M/C - 1):0];

reg[2:0] regI = 0; // Circulant memory address currently being processed
integer dataI = 0; // Data bit currently being processed

initial
begin
	parity = 0;
end

integer j = 0;

always @(posedge clk)
begin : LOOP
	if (start) begin
		state = FETCH;
		regI = 0;
		dataI = 0;
		parity = 0;
		//circulants[0] = 0;
		//circulants[1] = 0;
		//circulants[2] = 0;
		//circulants[3] = 0;
	end
	else begin
		if (state == FETCH) begin : PROC_FETCH
			integer i;
		
			//clkg = 0;
			//clkm = 0;
			
			douta = doutg;
			//douta = 64'hffffffffffffffff;
			
			//circulants[0] = douta;
			
			case (regI)
				2'd0 : begin
					for (i = 0; i < CIRCULANT_WIDTH; i = i + 1) begin
						circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 0 - 1) + i / C][i % C] = douta[i];
					end
					//circulants[0] = douta;
				end
				2'd1 : begin
					for (i = 0; i < CIRCULANT_WIDTH; i = i + 1) begin
						circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 1 - 1) + i / C][i % C] = douta[i];
					end
					//circulants[1] = douta;
				end
				2'd2 : begin
					for (i = 0; i < CIRCULANT_WIDTH; i = i + 1) begin
						circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 2 - 1) + i / C][i % C] = douta[i];
					end
					//circulants[2] = douta;
				end
				default : begin
					for (i = 0; i < CIRCULANT_WIDTH; i = i + 1) begin
						circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 3 - 1) + i / C][i % C] = douta[i];
					end
					//circulants[3] = douta;
				end
			endcase
			//circulants[0] = douta | regI | addrg;
			
			/*
			for (i = 0; i < CIRCULANT_WIDTH; i = i + 1) begin
				$display("Why is this not working? %d %d", regI, i);
				
					2'd0 : 
					2'd1 : circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 1 - 1) + i / C][i % C] = 32'hffffffff;
					2'd2 : circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 2 - 1) + i / C][i % C] = 32'hffffffff;
					default : circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - 3 - 1) + i / C][i % C] = 32'hffffffff;
				endcase
			//	circulants[CIRCULANT_WIDTH/C * (M/CIRCULANT_WIDTH - regI - 1) + i / C][i % C] = douta[i];
					//	circulants[i / C][i % C] = 32'hffffffff;
			end
			*/

			
			addrg = addrg + 1;
			regI = regI + 1;
			
			if (regI == M/CIRCULANT_WIDTH) begin
				state = PROCESS;
			end
		end
		else if (state == PROCESS) begin	: PROC_PROCESS
			integer i;
			//clkg = 0;
			//clkm = 0;
		
			if (doutm[DATA_WIDTH - dataI % DATA_WIDTH - 1]) begin
				// Add the circulants to the parity bits
			//parity[0] = parity[0] ^ 1;
				for (j = 0; j < M; j = j + 1) begin
					parity[j] = parity[j] ^ circulants[j/C][j%C];
				end
			end
			
			dataI = dataI + 1;
			
			if (dataI == K) begin
				// We are done
				state = IDLE;
			end
			else if (dataI % 32 == 0) begin
				// Load new circulants
				state = FETCH;
				regI = 0;
			end
			else begin : PROC_PROCESS_IDLE
				integer i;
			
				// Cyclic shift of the circulants
				for (i = 0; i < M/C; i = i + 1) begin
					circulants[i] = {circulants[i][0], circulants[i][31:1]};
				end
			end
			
		end
	end
end

always @(negedge clk) begin
	if (dataI % DATA_WIDTH == 0) begin
		addrd <= dataI / DATA_WIDTH;
	end
end

assign clkg = (state == FETCH) ? !clk : 0;
assign clkm = (dataI % DATA_WIDTH == 0) ? !clk : 0;

always @(negedge clk or posedge clk) begin
//	clkg <= (state == FETCH) ? !clk : 0;
/*
	if (clk == 1'b1) begin // Positive edge
		clkg <= 0;
		clkm <= 0;
	end
	else begin // Negative edge
		if (state == FETCH) begin
			clkg <= 1;
		end
		
		if (dataI % DATA_WIDTH == 0) begin
			addrd <= dataI / DATA_WIDTH;
			clkm <= 1;
		end
	end */
end

endmodule

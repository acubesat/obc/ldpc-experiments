`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:46:57 12/24/2018
// Design Name:   ldpc3
// Module Name:   /home/kongr45gpen/fpga/ldpc1/ldpc_3_statemachine_test.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ldpc3
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ldpc_3_statemachine_test;

	// Constants
	//param string = "LDPC is a parity check code relying on sparse parity-check matrices. Parity check codes add parity bits to the outgoing message.";
	reg [63:0] string [15:0];
	
	// Inputs
	reg clk;
	reg start;

	// Outputs
	wire [255:0] parity;

	// Instantiate the Unit Under Test (UUT)
	ldpc3 uut (
		.clk(clk), 
		.start(start),
		.parity(parity)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		start = 0;

		uut.addrd = 0;
		uut.weam = 1;
		repeat(16) begin
			case (uut.addrd)
				16'h0: uut.dinm = 64'h4c44504320697320;
				16'h1: uut.dinm = 64'h6120706172697479;
				16'h2: uut.dinm = 64'h20636865636b2063;
				16'h3: uut.dinm = 64'h6f64652072656c79;
				16'h4: uut.dinm = 64'h696e67206f6e2073;
				16'h5: uut.dinm = 64'h7061727365207061;
				16'h6: uut.dinm = 64'h726974792d636865;
				16'h7: uut.dinm = 64'h636b206d61747269;
				16'h8: uut.dinm = 64'h6365732e20506172;
				16'h9: uut.dinm = 64'h6974792063686563;
				16'ha: uut.dinm = 64'h6b20636f64657320;
				16'hb: uut.dinm = 64'h6164642070617269;
				16'hc: uut.dinm = 64'h7479206269747320;
				16'hd: uut.dinm = 64'h746f20746865206f;
				16'he: uut.dinm = 64'h7574676f696e6720;
				default: uut.dinm = 64'h6d6573736167652e;
			endcase
			#1 uut.clkm = 1;
			#1 uut.clkm = 0;
			uut.addrd = uut.addrd + 1;
		end
		uut.weam = 0;
		
		//$finish;
		
		#2;
		start = 1;
        
		// Add stimulus here
		repeat (20000) begin
			#1 clk = !clk;
		end
		
		$finish;
	end
      
endmodule


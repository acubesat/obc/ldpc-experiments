`timescale 1ns / 1ps
`include "ldpc_2.v"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    03:57:40 12/23/2018 
// Design Name: 
// Module Name:    ldpc_2_tester 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ldpc_2_tester(
    input CLK,
    input[63:0] data, // Input data for storage
	 input[3:0] addra, // Input address for storage
	 input wea, // 0 to read, 1 to write
	 input [31:0] addrp, // Address for parity
    output [7:0] parityo // Output for parity
    );

wire [63:0] dataReg;
wire [7:0] parity;

ldpc_2 ldpc_2_impl(CLK, douta, addrm, parity);

datamem datamemory(
	.clka(CLK), 
	.wea(wea), 
	.addra(maddr), 
	.dina(dina), 
	.douta(douta)
);

assign dataReg = (!wea) ? douta : 0;
assign parityo = parity[addrp];
assign maddr = (!wea) ? addra : addrm;

integer i = 0;
reg mode = 0;


always @(posedge CLK)
begin
	//2parity = douta;
end

endmodule

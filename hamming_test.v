`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:18:10 12/08/2018
// Design Name:   hamming
// Module Name:   /home/kongr45gpen/fpga/ldpc1/hamming_test.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: hamming
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module hamming_test;

	// Inputs
	reg CLK;
	reg [0:3] data;

	// Outputs
	wire [0:2] parity;

	// Instantiate the Unit Under Test (UUT)
	hamming uut (
		.CLK(CLK), 
		.data(data), 
		.parity(parity)
	);
	
	always #1 CLK = CLK ^ 1;
	
	always #4 data = data + 1;

	initial begin
		// Initialize Inputs
		CLK = 0;
		data = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		$finish;
	end
      
endmodule


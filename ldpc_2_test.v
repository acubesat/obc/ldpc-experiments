`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:38:33 12/23/2018
// Design Name:   ldpc_2
// Module Name:   /home/kongr45gpen/fpga/ldpc1/ldpc_2_test.v
// Project Name:  ldpc1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ldpc_2
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ldpc_2_test;

	// Inputs
	reg CLK;
	reg [1024:0] data = "Developing test LDPC algorithms. Developing testing LDPC algorithms. Developing test LDPC algorithms. Developing LDPC algorithms";

	// Outputs
	wire [256:0] parity;

	// Instantiate the Unit Under Test (UUT)
	ldpc_2 uut (
		.CLK(CLK), 
		.data(data),
		.parity(parity)
	);
	
	always #1 CLK = CLK ^ 1;

	initial begin
		// Initialize Inputs
		CLK = 0;

		// Wait 100 ns for global reset to finish
		#500;
        
		// Add stimulus here
		$finish;
	end
      
endmodule


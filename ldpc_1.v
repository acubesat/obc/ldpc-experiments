`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    04:38:06 12/08/2018 
// Design Name: 
// Module Name:    ldpc_1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module ldpc_1(
    input CLK,
    input [0:1023] data,
    output reg [0:255] parity
    );

reg[0:255] Grow;

integer i;
integer j;

parameter K = 12;
parameter P = 10;

reg[10:0] counter = 0;

always @(posedge CLK)
begin
	i = counter;
	
	Grow = i;
	parity[i] = 0;
	for (j = 0; j <= 25; j = j + 1)
	begin
		parity[j] = parity[j] ^ (Grow[j] && data[i]);
	end
	
	if (counter == 1023)
		counter = 0;
	else
		counter = counter + 1;
end

endmodule

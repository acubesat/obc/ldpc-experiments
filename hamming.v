`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:07:06 12/08/2018 
// Design Name: 
// Module Name:    hamming 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module hamming(
    input CLK,
    input [0:3] data,
    output reg [0:2] parity
    );

always @(posedge CLK)
begin
	parity[0] = data[0] ^ data[1] ^ data[3];
	parity[1] = data[0] ^ data[2] ^ data[3];
	parity[2] = data[1] ^ data[2] ^ data[3];
end

endmodule
